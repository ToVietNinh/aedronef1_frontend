import { Grid, makeStyles, Typography } from "@material-ui/core";
import Leftbar from "../components/Leftbar";
import Navbar from "../components/Navbar";
import PieChart from "../components/PieChart"
import BarChart from "../components/BarChart"
import Footer from "../components/Footer"

const useStyles = makeStyles((theme) => ({
  right: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  chartFrame: {
    marginTop: theme.spacing(10),
  },
  title: {
    fontSize: 20,
    marginBottom: theme.spacing(0),
    padding: theme.spacing(2),
    paddingLeft: theme.spacing(4.5),
  },

}));

const HomePage = () => {
  const classes = useStyles();
  const rateShipSuccess = [
    {
      type: 'Giao hàng thành công',
      value: 79,
    },
    {
      type: 'Giao hàng thất bại',
      value: 6,
    },
    {
      type: 'Khác',
      value: 15,
    },
  
  ];

  const userReview = [
    {
      type: 'Hài lòng',
      value: 75,
    },
    {
      type: 'Không hài lòng',
      value: 3,
    },
    {
      type: 'Khác',
      value: 12,
    },
  
  ];
  
  return (
    <div>
      <Navbar />
      <Grid container>
        <Grid item sm={2} xs={2}>
          <Leftbar />
        </Grid>
        <Grid container sm={10} xs={10} className={classes.chartFrame}>
          <Grid item sm={12} xs={12} style={{marginBottom: 50, padding: 20}} justifyContent="center">
            <Typography className={classes.title}>Số lượng đơn hàng theo tháng</Typography>
            <BarChart></BarChart>
          </Grid>
          <Grid item sm={6} xs={12} justifyContent="center">
            <Typography className={classes.title}>Tỷ lệ giao hàng thành công</Typography>
            <PieChart data={rateShipSuccess}></PieChart>
          </Grid>
          <Grid item sm={6} xs={12}>
            <Typography className={classes.title}>Đánh giá người dùng</Typography>
            <PieChart data={userReview}></PieChart>
          </Grid>
        </Grid>
       
      </Grid>
      <Footer/>
    </div>
  );
};

export default HomePage;

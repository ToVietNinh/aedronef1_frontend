import { Grid, makeStyles, Typography, Link, Button } from "@material-ui/core";
import { useState } from "react";
import Leftbar from "../components/Leftbar";
import Rightbar from "../components/Rightbar"
import Navbar from "../components/Navbar";
import TableOrder from "../components/TableOrder"
import MapBook from "../components/MapBook"
import Footer from "../components/Footer"

const useStyles = makeStyles((theme) => ({
  right: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  orderFrame: {
    marginTop: theme.spacing(9),
    marginBottom: theme.spacing(0),
  },
  orderTitle: {
    fontSize: 25,
    fontWeight: 800,
  },
  TableOrder: {
    margin: 15,
    marginTop: 0,
  }
}));

const ViewOrder = () => {
  const classes = useStyles();

  function createData(stt, name, quantity, unit, discount) {
    return {stt, name, quantity, unit, discount };
  }
  
  const orderData = [
    createData(1,'Frozen yoghurt', 1, 6000, 1000),
    createData(2,'Ice cream sandwich', 4, 9500, 1000),
    createData(3, 'Eclair', 2, 16000, 1000),
    createData(4, 'Cupcake', 1, 3700, 2000),
    createData(5, 'Gingerbread', 2, 18000, 1000),
  ];

  const [statusShip, setStatusShip] = useState(false)
  return (
    <div>
      <Navbar />
      <Grid container>
        <Grid item sm={2} xs={2}>
          <Leftbar />
        </Grid>
        <Grid container item sm={7} xs={10} direction="row" alignItems="flex-start" justifyContent="center" className={classes.orderFrame}>
          <Typography className={classes.orderTitle} color="black">Order #92023</Typography>
          <Grid container className={classes.TableOrder}>
            <TableOrder data={orderData}></TableOrder>
          </Grid>
          <MapBook></MapBook>
        </Grid>
        <Grid item sm={3} className={classes.right}>
          <Rightbar />
        </Grid>
      </Grid>
      <Footer></Footer>
    </div>
  );
};

export default ViewOrder;

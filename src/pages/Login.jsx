import React, { useState } from 'react'
import { Box, Grid, Paper, Avatar, TextField, Button, Typography, Link } from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Navigator, useNavigate } from 'react-router-dom';
import axios from 'axios';
async function loginUser(cre) {
    return axios.post("https://vast-garden-83097.herokuapp.com/api/auth/login",
        JSON.stringify(cre)
    ).then(res => {
        return res.data;
    }).catch(error => {
        return error.response.data;
    });
}

const LoginPage = () => {

    const paperStyle = { padding: 20, height: '70vh', width: 350, margin: "20px auto" }
    const avatarStyle = { backgroundColor: '#1bbd7e' }
    const btnstyle = { margin: '8px 0' }
    const [logboxStyle, setLogboxStyle] = useState();

    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [errorLogin, setErrorlogin] = useState();
    const navigate = useNavigate();
    
    const handleSubmit = async e => {
        e.preventDefault();
        const data = await loginUser({ username, password });
        if (data.status === "200") {
            localStorage.setItem('token', data.results.token);
            navigate('/');
            console.log(data.results.token);
        } else if (data.status === "400") {
            setErrorlogin(<div><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#D9212C" viewBox="0 0 16 16"><path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path><path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"></path></svg>Sai mật khẩu</div>);
            setLogboxStyle({ backgroundColor: '#ffc0c7', margin: "20px auto", borderRadius: '5px', color: '#282a35', padding: '10px'});
        } else if (data.status === "500") {
            setErrorlogin(<div><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#D9212C" viewBox="0 0 16 16"><path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path><path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"></path></svg>Tài khoản không tồn tại</div>);
            setLogboxStyle({ backgroundColor: '#ffc0c7', margin: "20px auto", borderRadius: '5px', color: '#282a35', padding: '10px'});
        }
    }

    return (
        <Grid>
            <Paper elevation={10} style={paperStyle}>
                <Grid align='center'>
                    <Avatar style={avatarStyle}><LockOutlinedIcon /></Avatar>
                    <h2>Sign In</h2>
                </Grid>
                <form onSubmit={handleSubmit}>
                    <TextField label='Username' placeholder='Enter username' onChange={e => setUsername(e.target.value)} fullWidth required />
                    <TextField label='Password' placeholder='Enter password' onChange={e => setPassword(e.target.value)} type='password' fullWidth required />
                    <Box component="span" style={logboxStyle} sx={{ display: { xs: 'block', sm: 'block' } }}>{errorLogin}</Box>
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="checkedB"
                                color="primary"
                            />
                        }
                        label="Remember me"
                    />
                    <Button type='submit' color='primary' variant="contained" style={btnstyle} fullWidth>Sign in</Button>
                </form>
                <Typography >
                    <Link href="#" >
                        Forgot password ?
                    </Link>
                </Typography>
                <Typography > Do you have an account ?
                    <Link href="#" >
                        Sign Up
                    </Link>
                </Typography>
            </Paper>
        </Grid>
    )
}

export default LoginPage
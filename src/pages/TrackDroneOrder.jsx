import { Grid, makeStyles, Typography } from "@material-ui/core";
import Leftbar from "../components/Leftbar";
import Navbar from "../components/Navbar";
import Rightbar from "../components/Rightbar";
import MapTrack from "../components/MapTrack"
import Footer from "../components/Footer"


const useStyles = makeStyles((theme) => ({
  right: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  mapFrame: {
    marginTop: theme.spacing(10),
    width: 200,
  },
  title: {
    fontSize: 25,
  },
}));

const ListOrders = () => {
  const classes = useStyles();
  return (
    <div>
      <Navbar />
      <Grid container>
        <Grid item sm={2} xs={2}>
          <Leftbar />
        </Grid>
        <Grid item sm={7} xs={10} className={classes.mapFrame}>
          <Typography className={classes.title}>Tracking Drone #1002 with order #92341</Typography>
          <MapTrack 
            
          ></MapTrack>
        </Grid>
        <Grid item sm={3} className={classes.right}>
          <Rightbar />
        </Grid>
      </Grid>
      <Footer></Footer>
    </div>
  );
};

export default ListOrders;

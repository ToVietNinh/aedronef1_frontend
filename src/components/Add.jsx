import {
    Button,
    Container,
    Fab,
    Input,
    makeStyles,
    Modal,
    Snackbar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Tooltip,
    Typography,
  } from "@material-ui/core";
  import { Add as AddIcon, Delete } from "@material-ui/icons";
  import { useState } from "react";
  import MuiAlert from "@material-ui/lab/Alert";
  
  const useStyles = makeStyles((theme) => ({
    fab: {
      position: "fixed",
      bottom: 20,
      right: 20,
    },
    container: {
      width: 800,
      height: 550,
      backgroundColor: "white",
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      margin: "auto",
      [theme.breakpoints.down("sm")]: {
        width: "100vw",
        height: "100vh",
      },
      overflow: "auto",
    },
    form: {
      padding: theme.spacing(2),
    },
    item: {
      marginBottom: theme.spacing(3),
    },
  }));
  
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  
  const Add = () => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [openAlert, setOpenAlert] = useState(false);
    const [quantityProduct, setQuantityProduct] = useState(2)
    const [summaryCost, setSummaryCost] = useState(0)
    const [totalCost, setTotalCost] = useState(0) 
    const [qty, setQty] = useState(1)
    const [unitCost, setUnitCost] = useState(0)
    const [discount, setDiscount] = useState(0)
  
    const handleClose = (event, reason) => {
      if (reason === "clickaway") {
        return;
      }
  
      setOpenAlert(false);
    };

    const handleDeleteRow = (array, rowIndex) => {
      return rowIndex
    }

    let forTextField = (qtyProduct) => {
        let componentRowField = []
        for (let i =1; i<= qtyProduct; i++) {
            componentRowField.push(
                <TableRow key={i}>
                    <TableCell>{i}</TableCell>
                    <TableCell align="right">
                        <div className={classes.item}>
                            <TextField></TextField>
                        </div>
                    </TableCell>
                    <TableCell align="right">
                        <div className={classes.item}>
                            <TextField 
                              type="number" 
                              onKeyPress={(event) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }} 
                            onChange={(e) => {setQty(e.target.value)}}>
                            </TextField>
                        </div>
                    </TableCell>
                    <TableCell align="right">
                        <div className={classes.item}>
                            <TextField
                              onKeyPress={(event) => {
                                if (!/[0-9]/.test(event.key)) {
                                  event.preventDefault();
                                }
                              }}
                              onChange={(e) => {setUnitCost(e.target.value)}}
                            ></TextField>
                        </div>
                    </TableCell>
                    <TableCell align="right">{unitCost * qty}</TableCell>
                    <TableCell align="right">
                        <div className={classes.item}>
                            <TextField
                              onKeyPress={(event) => {
                                if (!/[0-9]/.test(event.key)) {
                                  event.preventDefault();
                                }
                              }}
                              onChange={(e) => {setDiscount(e.target.value)}}
                            ></TextField>
                        </div>
                    </TableCell>
                    <TableCell align="right">{totalCost}</TableCell>
                    <TableCell align="center">
                      <Button
                        onClick={() => handleDeleteRow(i)}
                        style={{cursor:"pointer"}}
                      >
                        <Delete

                        ></Delete>
                      </Button>
                        
                      
                  </TableCell>
                </TableRow>
            ) 
        }
        return componentRowField
    }
    return (
      <>
        <Tooltip title="Add" aria-label="add" onClick={() => setOpen(true)}>
          <Fab color="primary" className={classes.fab}>
            <AddIcon />
          </Fab>
        </Tooltip>
        <Modal open={open}>
          <Container className={classes.container}>
            <form className={classes.form} autoComplete="off">
              <Typography variant="h5" style={{fontWeight: 700, marginBottom: 40}}>Create order</Typography>
              <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>STT</TableCell>
                            <TableCell align="right">Tên sản phẩm</TableCell>
                            <TableCell align="right">Số lượng</TableCell>
                            <TableCell align="right">Đơn giá</TableCell>
                            <TableCell align="right">Thành tiền</TableCell>
                            <TableCell align="right">Giảm giá</TableCell>
                            <TableCell align="right">Tổng</TableCell>
                            <TableCell align="right">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {forTextField(quantityProduct)}
                    </TableBody>
                </Table>
              </TableContainer>
              <div className={classes.item} style={{marginTop: 15}}>
                <Button
                  variant="outlined"
                  color="primary"
                  style={{ marginRight: 20 }}
                  onClick={() => setOpenAlert(true)}
                >
                  Create
                </Button>
                <Button
                  variant="outlined"
                  color="secondary"
                  style={{ marginRight: 20 }}
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </Button>
                <Button
                  variant="outlined"
                  style={{ marginRight: 20, color: "#4caf50"}}
                  onClick={() => setQuantityProduct(quantityProduct+1)}
                >
                  Add Row
                </Button>
              </div>
            </form>
          </Container>
        </Modal>
        <Snackbar
          open={openAlert}
          autoHideDuration={4000}
          onClose={handleClose}
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
        >
          <Alert onClose={handleClose} severity="success">
            Order created successful!
          </Alert>
        </Snackbar>
      </>
    );
  };
export default Add;
  
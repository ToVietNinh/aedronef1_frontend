import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  makeStyles,
  Typography,
} from "@material-ui/core";

import DeleteOrder from "./DeleteOrder"

import {
  RateReview,
} from "@material-ui/icons"
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: theme.spacing(5),
  },
  media: {
    height: 250,
    [theme.breakpoints.down("sm")]: {
      height: 150,
    },
  },
}));


const Post = ({title }) => {
  const classes = useStyles();
  let navigate = useNavigate()

  const redirectToViewOrder = () => {
  let path = "/order/view?id=2"
  navigate(path)
}
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardContent onClick={redirectToViewOrder}>
          <Typography gutterBottom variant="h5">
            {title}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={redirectToViewOrder}>
          <RateReview color="primary"></RateReview>
            View
        </Button>
        <DeleteOrder></DeleteOrder>
      </CardActions>
    </Card>
  );
};

export default Post;

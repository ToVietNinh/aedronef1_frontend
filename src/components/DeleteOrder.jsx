import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Delete } from '@material-ui/icons';
import { Snackbar, Typography } from '@material-ui/core';
import { Alert } from '@material-ui/lab';


function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const DeleteModal = () => {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [openAlert, setOpenAlert] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <Typography style={{padding: 2, fontSize: 22}}>
        Are you sure want to delete order #15
      </Typography>
      <button onClick={()=> {setOpenAlert(true)}}>Delete</button>
      <button>Cancel</button>
    </div>
  );

  return (
    <div >
      <button size="small" color="primary" onClick={handleOpen} 
        style={{border:"none", backgroundColor: "white", cursor: "pointer"}}
      >
        <Delete color="primary"></Delete>
            <span color="primary" style={{color: "#3f51b5"}}>DELETE</span>
      </button>
      
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
      <Snackbar
        open={openAlert}
        autoHideDuration={4000}
        onClose={handleClose}
        anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
      >
        <Alert onClose={false} severity="success">
          Order #15 delete successfully!
        </Alert>
      </Snackbar>
    </div>
  );
}

export default DeleteModal
import React from 'react';
import { Bar } from '@ant-design/plots';

const BarChart = () => {
  const data = [
    {
      type: 'Tháng 1',
      sales: 38,
    },
    {
      type: 'Tháng 2',
      sales: 52,
    },
    {
      type: 'Tháng 3',
      sales: 61,
    },
    {
      type: 'Tháng 4',
      sales: 90,
    },
    {
      type: 'Tháng 5',
      sales: 48,
    },
    {
      type: 'Tháng 6',
      sales: 38,
    },
    {
      type: 'Tháng 7',
      sales: 12,
    },
    {
      type: 'Tháng 8',
      sales: 38,
    },
    {
        type: 'Tháng 9',
        sales: 23,
    },
    {
        type: 'Tháng 10',
        sales: 13,
    },
    {
        type: 'Tháng 11',
        sales: 38,
    },
    {
        type: 'Tháng 12',
        sales: 20,
    },
  ];
  const config = {
    data,
    xField: 'sales',
    yField: 'type',
    seriesField: 'type',
    color: ({ type }) => {
      return type === 'Tháng 4' ? '#FAAD14' : '#5B8FF9';
    },
    legend: false,
    meta: {
      type: {
        alias: '类别',
      },
      sales: {
        alias: '销售额',
      },
    },
  };
  return <Bar {...config} />;
};

export default BarChart
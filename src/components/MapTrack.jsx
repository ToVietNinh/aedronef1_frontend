import { Box, Grid, Typography } from '@material-ui/core';
import { GoogleMap, Marker, Polyline, useJsApiLoader } from '@react-google-maps/api';
import React, { useState } from "react";
import { useEffect } from 'react';
const containerStyle = {
  width: '950px',
  height: '800px'
};
const config = require("../config")


const MapTrack = () => {
	const [currentLat, setCurrentLat] = useState(0)
	const [currentLng, setCurrentLng] = useState(0)
	
	if (navigator.geolocation) {

		navigator.geolocation.getCurrentPosition(function(position) {
				
			setCurrentLat(position.coords.latitude)
		
			setCurrentLng(position.coords.longitude)
		});
	}
    const libraries = ['places']

	const {isLoaded, loadError} = useJsApiLoader({
        googleMapsApiKey: config.env.REACT_APP_GOOGLE_API_KEY,
        libraries,
    })

	const path = [
		{lat: 20.990958, lng: 105.517902},
		{lat: 20.993999, lng: 105.517902},
		{lat: 20.993999, lng: 105.529090},
		{lat: 20.995200, lng: 105.529090},
		{lat: 20.999800, lng: 105.519290}
	];

	const onLoad = polyline => {
		console.log('polyline: ', polyline)
	};

	const options = {
		strokeColor: '#FF0000',
		strokeOpacity: 0.8,
		strokeWeight: 5,
		fillColor: '#FF0000',
		fillOpacity: 0.35,
		clickable: false,
		draggable: false,
		editable: false,
		visible: true,
		radius: 30000,
		
		zIndex: 1
	};

	const [statusShip, setStatusShip] = useState("Đơn hàng đang được giao bởi Drone #9823!")
	const [statusShipColor, setStatusShipColor] = useState("red")
	const [lat, setLat] = useState(20.990958)
	const [lng, setLng] = useState(105.517890)
	const [count, setCount] = useState(0)
	const [stastic, setStastic] = useState(0)

	useEffect(() => {

		const intervalEnd = setInterval(() => {
			setStatusShip("Đơn hàng đã đến nơi. Vui lòng ra nhận hàng!")
			setStatusShipColor("green")
			setLat(20.999800)
			setLng(105.519290)
			setCount(count+1)
		}, 10000)

		return () => {clearInterval(intervalEnd)}
		
	}, [stastic])


    return (
        
			isLoaded ? 
				<Box>
					<GoogleMap
						mapContainerStyle={containerStyle}
						center={{
							lat: lat,
							lng: lng
						}}
						zoom={10}
						>
						{ /* Child components, such as markers, info windows, etc. */ }
						<Marker
							style={{color:"red"}}
							position={{
								lat: lat,
								lng: lng
							}}
							icon={{
								url: "https://static.thenounproject.com/png/103844-200.png",
								scale: 0.7,
								anchor: new window.google.maps.Point(20, 20),
								scaledSize: new window.google.maps.Size(60, 60),
							}}
							animation={1}
						/>
						<Marker
							style={{color:"red"}}
							position={{
								lat: 20.999800, 
								lng: 105.519290
							}}
						/>
						<Marker
							style={{color:"white"}}
							position={{
								lat: 20.990958, 
								lng: 105.517902
							}}
							
							icon={{
								url: "https://cdn-icons-png.flaticon.com/512/1239/1239344.png",
								scale: 0.7,
								anchor: new window.google.maps.Point(13, 20),
								scaledSize: new window.google.maps.Size(30, 30),
							}}
						/>
						<Polyline
							onLoad={onLoad}
							path={path}
							options={options}
						/>
					</GoogleMap>
					<Grid container justifyContent='center' style={{padding: 25}}>
						<Typography style={{color: statusShipColor, fontSize: 22, fontWeight: 800}}>{statusShip}</Typography>
					</Grid>
				</Box> : <></>
		
	)
}

export default MapTrack;
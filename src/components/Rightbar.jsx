import {
  Container,
  ImageList,
  ImageListItem,
  makeStyles,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(10),
    position: "sticky",
    top: 0,
  },
  title: {
    fontSize: 20,
    fontWeight: 500,
    color: "#555",
  },
  link: {
    marginRight: theme.spacing(2),
    color: "#555",
    fontSize: 16,
  },
}));

const Rightbar = () => {
  const classes = useStyles();
  return (
    <Container className={classes.container}>
      <Typography className={classes.title} gutterBottom>
        Transporting by Drone
      </Typography>
      <ImageList rowHeight={150} style={{ marginBottom: 20 }} cols={2}>
        <ImageListItem>
          <img
            src="https://d1e00ek4ebabms.cloudfront.net/production/45e9d39d-e090-479c-8090-33172a020927.jpg"
            alt=""
          />
        </ImageListItem>
        <ImageListItem>
          <img
            src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBEREBEPEREREA8PEg8PDw8REREPDw8PGBQZGRgUGBgcIS8lHB4rHxgYJjgmLS8xNTU1GiQ7QDszPy40NjEBDAwMEA8QHhISHjQhJSQ0NDQ0NDExNDQ0MTQxNDE0NDE0NDQ0NDQ0NDQxNDQ0NDQ0NDQ0NDQ0NDExNTQxND80NP/AABEIAKkBKwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAQIDBQQGB//EAEQQAAICAQIDBAcDBwkJAAAAAAECABEDBBIFITEGE0FRIjJhcZGhwRSBsUNSYpKistIVQlNjcoKTwtEHIyREc4Oj4fD/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAAfEQADAQADAAMBAQAAAAAAAAAAARECAxIhEzFBUSL/2gAMAwEAAhEDEQA/AKRCMCFT6J84UI6hUlJAhCo6iiChJQiiBCEIogQhCKIEIRxRAhCEUQIQhFECEIRRBRwhAgoRwgQUI4QIKEIRRAkZKEUQjCShFEIxRwiiESJEiTMRECFREjUsMjUCF8I6hUhRwhUKilgRxVCpKIOEVQqKIOEVQqKIOoVCEUQKhUIRRAqFRwgQVQqOElAqhUcJaBQjhJQRqOOFRQKKOoVFKKEdRVFAQhUKikgQhUVRRBQjqFRRCJgY6iIiiEDFJESFRSw6YQhAgQhHBYKOTxoCaLKntYMR8gZ0po1P5fCPeci/isNlWTjhNJOEFvVz6Zvdl5/hOhezWoPMNiI8w7EfuydkXo/4YsJuDsxqPzsX6z/wxY+z+RXRcj4tpPNRl2Ow8hak/KTvn+jo/wCGJCa/H+HY9OysHVEYH0N2TMbHiCEHL3zEOr04/K2fJcbH8ak+Rf0vx6/hZCpWNbh8Gyt/ZxX/AJo/teLxx6o/9oD6mT5EPj0TqFTiz8XVT6OmysP0soQ/DZLNJxVNytl0rpi3KGY5weRPlsBPuk+TJfiZ1FSKsVfMe0eYiqeq4/xPTLgwvmw5TgcBkyb/AFBX5oYtfh0nNi1HDh+Rzk+RwZ3+hhcqn0afC79nnqhPRtq9P+T0jX4b9NnQfHYRNHBpGfTs4GPHlNlEVU2+wG+cfIifEzxoxseik+4ExnC/5jfqmbD4NceoI9gbGv4GUPpdV/Oxs3vcH/NNdjPQyqhNH7K464D9zSQxoPWwZB7jctJ1MyE11x6Q+t3ie+x9JZo8mhx50UPky5GICY1UMbPIHmPYZHqIqw2zEhOztfxDDi1GxcWV8pW8ib8eLYfC6Q2ecw8fE2bppMlf9Td/kEyuRGnxM0IShNQx/wCXyD35McsDk/kyPe6/QTa0mYeYSuEQ91f3r+klLSQUVwqOKIK5Ex1AiKIRMjJGRqKIXwiuFyUsJQihFEHCK47iiBJo7KbUlT5qSp+IkLhFEO1uKagrtObIV8t5/HrMlsxXV6YEja5fcGplY2vUHkes6ZoY8XCHGM6zcHRQbyPkRA59aitA9AfGc9xZOnGm9C7TsPtOILsA9UqgVBWxjVLOXFoWyn0MRyH2Jv8AnU9PpdRwLf3iZNKzqBzfMWA8OjGvlPTYtdiKju2QpXLYQVr2VOS5Oqh2fH2dPD6bsxq2/mLjX9NlHyWzNPB2OP5TN9yJ9SfpPUfaR5w+0DzkfKwuJGPg7K6VfWGR/azkfu1MntzwfEnD8j4sSh0yYHsC3294oNE8zyJ5eM9d3wngeO/7R+HbKwapXyY3vacWo2vV9GCUedHrR85ns2aWUvwp7Ru78MxKuHMWKivQFg14i7HwmxwbiODIu1AVyLydHFOG8ffM09vqdEONt1bwpGPFaOlo9h25EWfunDj4/iyN9oXYjsbcrt5n3+Mudfga/T6FpnBneqKfATz3CuILkUNYubuFuXWZZom+mQ/zV+AnHqeF43HpY/1WdP3SJob68ZFs/t+cWElPM6nsxjbmrZE/vlh+1cx9T2ZzD1Cr+xrU/We2bUfpTkzcb02P18+FK678iLXxM2uTSMvjy/w8Q/A9Wos4HI812v8AIG/lM7Sf7viGJsiONgTcuxtw5t4Ge91HavSKneLlTIvOu5ZchY+Q29ZlP27VfSXSaph7sQb4F7l+TWlIZXHnLtPL8f1K5OKF0V1Tu3FuhS+awnoM/bI51YDRsjbSEbUd2wUnqQoJP4Tz868bcOfIlfGKEITdOUCEIQIKEcUCBIyUjAgjIyRkZaQuiqOEyaCoRwgooRwgChHCAKBF8jzEcIBFQB0AHuFQVABQAA6laG0nzI6EyUJGkVNoasBzUMjjmrJm1KU3gdofaR7Kqel7O5M+o373HobaKrtu76iz5TzM9n2OQLp8jnkDkbn7Ao/9zlyZylTrx709Q5dfxn7JlGLMwG4bkfwK3XPynldavDTletBpnLHcXNjeWFk0OnMzp4jqe/yvkYWHJpTzAQclWvd9Z5/iPB2Yh9O4R7pkYkJt8xQJu5j43PDfy5sYuJarG2S1xojBUT0aO1UsIAavbXKvfNzWcCx6fh+HOSpz5+6fapsIHG5RX9kHnPItwbWBy9o5sAhX5st9AGAE9Z2+xrw7Bhx4+WbWppl1ORizll0qAIEF0vN+tdBI019mk0yHCuIPjbmQEJNWanttDxdSo9NP11/1nyTQ69cxr1yoG5jzVR900VdjXcp3i8wXBCoCPC26/dJb+F+v0+qPxJAu7epHmCCJzcN41izZu6D3yNdaYjwB6Ez59ptK/Nsjlt3XGprEPu6sffy9k7sTlGV0NMhDL7CPpNrjbXpzfKr4e07WsceFTjbY5ZAxUgsqHd6VHwJFXPHDVOPV2IT1bHjx43Y+ZZVBJnqeJ6RMmlza5LOTPh06OTZrEmTfsq65Fm51c8hNcaUM8jdJu5Y7mJZjyLMSxI95kLhCdTiFwuKEUQcLkLhcUQnCQuFxRCcUjcLiiEpEwuK4ogGRjJkblohfCEJksCEIQIOEUIKEIQgQcIoQIOEULgQc9S2c4eGKo5HKCB5+mST+zc8oTPSdp/QxaTF5JuI9yqB+JmNetI3nxNnnY7kbhc2YhZiFso82UfOX/wC084G12jTOyomLT5nIawpLsgAJ9yt8JXoRebEPPJjHxcQ7fcC+3cUcM7omHR6eggBLOcj+jR68iT905cjO3GQ4B2i0mPSqO8CIhOIDHpcrIGAurF3y534wy5+8Y5AbV6ZTt2ErXL0fCPhvY/Dj03dNkz13hyEnYr7qAAoqeVfjObjms03D8WIOMjtk3LjQOoOxQAWZtvLqB05mYw0ma2m1C2E5tDrUz41y492xrBVqDo46o1cr5g+0EGdFz0J087UPYdmH73SajTHwDqPYrqa+e6ePnouxWatQ6eD4yfvVhX4mYWqTbkyL+bkdfgxExnxtG9e5TKoRXCbMQcUVxXAJQkbhcCBCFwuBBQhCBAijigCMjGTFALrjkbhcCEoSNwuBCUchcLgpOEhcLkBKEjcLgDuFxXC4ohNBZA8yB856Htq//EongmJPiWb/AEE85jfayt12kGvOjc9R2j0L6nMmo0+3JjfGnMOi7SL5ekR7Jh/aNpf5Z5iK5o/yHqv6L9vF/FD+Q9V/Rf8Akx/xTXZGerKeFc9TgH9bi/fE0+1HEV0+rz5GDHYEb0RZ2jEtg/P4yvhvCc2PPifIgREdXZy6EAKb8D7Jn8d1KajUZnFNjyHaL5h0ChfgQPnM6S04dMt5R0YeM5c+lbOmNghBoN65VaO4L5c/lPEdoHHEVxNiZTn029GxMyocmNmDB0LEA0bBHWiJtvjKI4xtkU0CAuTILI8PW8uU6cnA9OiI7vovTRHJdsYcuygsKIskG/fOaxH9mu9X0ZnZ7QNp8AR63s7ZGUEMEtUULY5E0oPLz9k1LnRodH36b8WTCyAlOb7CCPCmAPlOkcGyfn4P8VZ0TS8pzeW/YXdlsm3WYv0t6/sMfpOPi4rU5x/W5f3zNDhmjOmzY8+bJiXHj3OSMisb2kAfOZGv1PeZcmQCg+R3APUAmxCd14GpkquFyNwubMErhchcLgQlcLkLhcAlcLkbiuATuK5G4XAJXETI3FcAZMjcCYrlBdcdyFwuQsJ3C5C4XBSdwuQuFyCE7hchcW6Z7r6NLjbVLLhcr3R7pqkjJ3C5XcuTHYmXpIqy2QuVPgVjZ3X+i+RB8FIlripDdLUyRoh9lTzf/Gy/xQ+zL+dk/wAbN/FJ7o5lvKNZzpsrGnXzdvY+TI6/BiRLrkOcLPlGdZf0NY0vsnc58miwsxZseNmPViilj7zLecLmqidWUrocI6Y8Y9ygRnR4vFE+E7cOO4tRj2zK1luFeWlTkTS41O5URWHQhRYltyAaOz5TVROrJXC5HnCpl8mU4bXDpqkriuIgxc5rsjHRkrhcjuksC7jJrSQzhscjcvypQucge4zpNUPDThZcVxG5CjJrayXPFrTLLiJgFMUZ2tfQ3xaz9gTI3ExkbnQ5wvuFxAwJkNDuFxXC4A7idoXItJr6LlVlmPnLRjkcAlzmhPl72+59bGF0Id3KnSvGceq1e3xnE3Eb8TPVjTaPHvKTNYMPOd+HKtdZ5PPriOlyrFxdrrnNb9Oa8PSazKL6yvHkEye+Li5yPrSpoXLfBIz0iODL7E8zptU5bxm/gNgec83Nto9PDml4WMgeMYWZnEcxQGc+PbsO3JhPNOx2XzlRcCedTihY1RMsyat66ET2rw8LVPVaTUqI9fmBHKeHPGHU1NrQalsigmYa6uml74duJxctZx/8Zh67UnGT8pVi1LsBK9eUJew9GjiWLRmZow58JqotTx8jdPdxpQGIHWc75k85DXEgcp5vLrHDkfCd+HbaPPzZWWeibIJZptUAfCefXM7LOHPrHQ9Z2bqh55PT2us1QKeEy8ecX4TH0muZ+pktRlKgmTtPDSzfTefUDzEsxZgTyozyuLVO81tErcucxyeo6cTaZuKblWSNFNSvJM8DjN867ZIMZGJ3kN09vZHgeGdQMRM5MOosQy6ipO6J1Z1XItkAnAdVOXNqjcPaLDY70ecrOoFzGOqNSCagmZ1vw1lenpsOpEvbUKRXKeWXUN0HWdCu/nPHrjTdPYuVpQv15Blek04Y9JzuWYgGbPDMRUTcaz4YTWtenHquH7gaEyBoSr17Z7sIGEzNTpwGuc1rX6b3jL+jhwYPQ6TkbhrM90Zt4yB1nTjyLcvyakQWc/pm49DsW65yS6oLyvpNPUgFZ5vWIVLGZylr7Nabz9Gp9vHnOHiWYMLmXiZmPUzqzISleM0s5TM99NGRp9u8++ehx6XenSY2k0LF7rxnsdBj2oLm+TX8M8a/p5XVcBJboZucK4eUUCuk0c2oUGWabKCOU5a1qem1nN8MXX8N3npLtPwwKBYmlmejJB9wqZ7ahtdUzKfIMcieJDzkOK4CQa98xcWmfdzup0ystVnPW9JxGxn1e4TA1Dgvc1TpyJxrpLfp4zeXlGNtv7O3QYt46S7U8G3j1Z3cO04WvjO7NnUDwnPWn28O2Ys+mBpuFd34SWXQ7uVTRfUWZQuXnMvWr6T/ADPCjTcMCjpLHIQ8p2d9yqcWfFfORa99DfnhNdVyiOazKTi5RIOYEvZfhiv9DKxle+dgxWDKjhmlyMy804dLdSeVTJaSW5p0enTl+HEUlD452NK2k7MjOPuzLMODnLl6y/F1+EPTGR6bS+lNNdKKlOn6iaC9JyemenP0cODR7j08Zt4+HPtFfhKuH9Z6PB6onow/Di/sy9Pw/IOpPwnJr9G09OszOIzOzedM81kxmcu1lY8zNPJ9ZyanqZ51pmxrqOQBM5tam5T7ZHxluX1fhKmV/RmY8YWdWFLMryeEu08dmYRemMKZ0HLyq5Q3WD9JOzppfRJMO7nc1dDpqE4dHNnTSb0zWUcGtxc5TjG0Tt1c42k7OD9IuobrOV9MAZ1pK36/fHZkZzZMcjjw+ydLxJ9IWmQiWIqc+azLz1lbyorK0xyQTlJrGvSKQ5yaIEuY8pU/rSw9JCliqCJRs5yxekIIy3GRVSpyLMS9ZDJ1MEP/2Q=="
            alt=""
          />
        </ImageListItem>
        <ImageListItem>
          <img
            src="https://store.dji.com/guides/wp-content/uploads/2021/11/%E5%8D%96%E7%82%B92-scaled.jpg"
            alt=""
          />
        </ImageListItem>
        <ImageListItem>
          <img
            src="https://cdn.brvn.vn/editor/2018/09/Xuhuonggiaohangtudong1_1537811999.jpg"
            alt=""
          />
        </ImageListItem>
        <ImageListItem>
          <img
            src="https://media.doanhnhantrevietnam.vn/files/content/2022/07/22/doanh-nhan-tre-vietnam-cong-nghe-dung-drone-giao-hang-dang-bi-thu-thach-hinh-anh-1-1056.jpg"
            alt=""
          />
        </ImageListItem>
        <ImageListItem>
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/DJI_Phantom_2_Vision%2B_V3_hovering_over_Weissfluhjoch_%28cropped%29.jpg/1200px-DJI_Phantom_2_Vision%2B_V3_hovering_over_Weissfluhjoch_%28cropped%29.jpg"
            alt=""
          />
        </ImageListItem>
        <ImageListItem>
          <img
            src="https://www.cnet.com/a/img/resize/cd8bb029571295a81b18da26c41c758658bb65f8/hub/2020/11/04/36515bdc-bfe9-499e-99eb-03cfad617999/dji-mini-2-01.jpg?auto=webp&fit=crop&height=528&width=940"
            alt=""
          />
        </ImageListItem>
        <ImageListItem>
          <img
            src="https://cdn.tgdd.vn/hoi-dap/1333563/Thumbnail/drone-uav-la-gi-ung-dung-cua-phuong-tien-bay-khong-nguoithumb1111234.jpg"
            alt=""
          />
        </ImageListItem>
      </ImageList>
    </Container>
  );
};

export default Rightbar;

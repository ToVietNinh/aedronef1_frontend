import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function TableOrder({data}) {
  const classes = useStyles();

  const totalMoney = () => {
    let total = 0.0
    data.forEach(item => {
      total += (item.quantity * item.unit) - item.discount
    });

    return total

  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>STT</TableCell>
            <TableCell align="right">Tên sản phẩm</TableCell>
            <TableCell align="right">Số lượng</TableCell>
            <TableCell align="right">Đơn giá</TableCell>
            <TableCell align="right">Thành tiền</TableCell>
            <TableCell align="right">Giảm giá</TableCell>
            <TableCell align="right">Tổng</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((item) => (
            <TableRow key={item.name}>
              <TableCell>{item.stt}</TableCell>
              <TableCell align="right" component="th" scope="row"> {item.name} </TableCell>
              <TableCell align="right">{item.quantity}</TableCell>
              <TableCell align="right">{item.unit}</TableCell>
              <TableCell align="right">{item.quantity * item.unit}</TableCell>
              <TableCell align="right">{item.discount}</TableCell>
              <TableCell align="right">{(item.quantity * item.unit) - item.discount}</TableCell>
            </TableRow>
          ))}
            <TableRow>
                <TableCell></TableCell>
                <TableCell colSpan={4}>Tổng</TableCell>
                <TableCell align="right">{totalMoney()}</TableCell>
            </TableRow>
        </TableBody>
        
      </Table>
    </TableContainer>
  );
}
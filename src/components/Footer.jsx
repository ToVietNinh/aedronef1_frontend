import { Box, Container, Grid, makeStyles, Paper, Typography } from '@material-ui/core'
import { Facebook, Instagram, Web } from '@material-ui/icons'
import {React} from 'react'

const useStyles = makeStyles(() => ({
    iconsocial: {
        fontWeight: 500,
        fontSize: 30
    },
    copyright: {
        paddingBottom: 15,
        marginBottom: 10,

    },  
    info: {
        fontSize: 15,
        fontWeight: 450,
        paddingBottom: 15
    },
}))

const Footer = () => {
    const classes = useStyles();
    return (
        <footer style={{zIndex: 999, marginTop: 50}}>
            <Box bgcolor="#555" color="white" padding={5}>
                <Container maxWidth="lg" >
                    <Grid container spacing={11}>
                        <Grid container xs={12} sm={12} justifyContent="center" className={classes.copyright}>
                            <Typography borderBottom={1} style={{fontWeight: 1000, fontSize: 21}}>Copyright @AEDroneF1 - UET - VNU</Typography>
                            
                        </Grid>
                        <Grid container xs={12} sm={6} justifyContent="center" marginLeft={100}>
                            <Typography borderBottom={1} className={classes.info}>Managed by University Of Engineer Technology - VNU</Typography>
                        </Grid>
                        <Grid container xs={12} sm={6} justifyContent="center">
                        <Typography borderBottom={1} className={classes.info}>SĐT: 0367171906</Typography>
                        </Grid>
                        <Grid container xs={12} sm={12} justifyContent="center">
                            <Facebook className={classes.iconsocial}></Facebook>
                            <Instagram className={classes.iconsocial}></Instagram>
                            <Web className={classes.iconsocial}></Web>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </footer>
    )
}

export default Footer
import { Box, Button, ButtonGroup,TextField, Link, Grid, Typography } from '@material-ui/core';
import {GoogleMap, Marker, useJsApiLoader } from '@react-google-maps/api';
import React, { useEffect, useState } from "react";
import LoadingOverlay from "react-loading-overlay-ts";
import styled, { css } from "styled-components";
import {Autocomplete} from "@material-ui/lab"
import { LocationOn } from '@material-ui/icons';
const config = require('../config')

const containerStyle = {
  width: '950px',
  height: '600px'
};


const MapBook = () => {

	const [currentLat, setCurrentLat] = useState(0)
	const [currentLng, setCurrentLng] = useState(0)

    const [originLocate, setOriginLocate] = useState('')
    const [destinationLocate, setDestinationLocate] = useState('')

    const [directionResponse, setDirectionResponse] = useState(null)
    const [distance, setDistance] = useState('')
    const [duration, setDuration] = useState('')

    const [openInfo, setOpenInfo] = useState(false)

	if (navigator.geolocation) {

		navigator.geolocation.getCurrentPosition(function(position) {
			setCurrentLat(position.coords.latitude)
			setCurrentLng(position.coords.longitude)
		});
	}

    const DarkBackground = styled.div`
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 999; /* Sit on top */
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        left: 0;
        right: 0;
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */

        ${props =>
            props.disappear &&
            css`
            display: block; /* show */
        `}
    `;

    // const calculateRoute = (e) => {
    //     e.preventDefault();
    //     console.log("originll")
    //     if (originLocate === '' || destinationLocate === '') {
    //         return  
    //     }
    //     // eslint-disable-next-line no-undef
    //     const directionService = new google.maps.DirectionsService()
    //     const result = directionService.route({
    //         origin: originLocate,
    //         destination: destinationLocate,
    //         // eslint-disable-next-line no-undef
    //         travelMode: google.maps.TravelMode.DRIVING,
    //     })
    //     setDirectionResponse(result)
    //     setDistance(result.routes[0].legs[0].distance.text)
    //     setDuration(result.routes[0].legs[0].duration.text)
    //     console.log(distance, duration)
    // }

    // const clearRoute = () => {
    //     setDirectionResponse(null)
    //     setDistance('')
    //     setDuration('')
    //     setOriginLocate('')
    //     setDestinationLocate('')
    // }

    const libraries = ['places']

    const {isLoaded, loadError} = useJsApiLoader({
        googleMapsApiKey: config.env.REACT_APP_GOOGLE_API_KEY,
        libraries,
    })
    const [statusShip, setStatusShip] = useState(false)
    const [loaded, setLoaded] = useState(true);
    useEffect(() => {
        if (!loaded) {
          setTimeout(() => setLoaded(true), 7000);
        }
    }, [loaded]);

    const locationList = [
        {id: 1, location: "Chùa Láng, Đống Đa, Hà Nội"},
        {id: 2, location: "Xuân Thuỷ, Cầu Giấy, Hà Nội"},
        {id: 3, location: "Ngã Tư Sở, Thanh Xuân, Hà Nội"},
        {id: 4, location: "Láng Hạ, Đống Đa, Hà Nội"},
        {id: 5, location: "Nguyễn Trãi, Thanh Xuân, Hà Nội"},
        {id: 6, location: "Mỹ Đình, Nam Từ Liêm, Hà Nội"},
        {id: 7, location: "Tạ Quang Bửu, Hai Bà Trưng, Hà Nội"},
        {id: 7, location: "Bạch Mai, Hai Bà Trưng, Hà Nội"},
        {id: 7, location: "Nhật Tân, Tây Hồ, Hà Nội"},
        {id: 7, location: "Trung Kính, Cầu Giấy, Hà Nội"},
        {id: 7, location: "Trung Hòa, Cầu Giấy, Hà Nội"},
        {id: 7, location: "Xuân Phương, Nam Từ Liêm, Hà Nội"},
        {id: 7, location: "Tôn Thất Thuyết, Cầu Giấy, Hà Nội"},
        {id: 7, location: "Phạm Văn Đồng, Bắc Từ Liêm, Hà Nội"},
        {id: 7, location: "Đại học Quốc gia Hà Nội - Cơ sở Hòa Lạc"},
        {id: 7, location: "Trung tâm Giáo dục Quốc phòng và An ninh ĐHQGHN"}
    ]

    return (
        <>
        <Box
            justifyContent='center'
            direction='column'
            style={{position:'relative'}}
        >   
         
                {
                    isLoaded ? 
                    <Box>
                        <GoogleMap
                        mapContainerStyle={containerStyle}
                        center={{
                            lat: currentLat,
                            lng: currentLng
                        }}
                        zoom={10}
                        >
                        <Marker
                                position={{
                                    lat: currentLat,
                                    lng: currentLng
                                }}
                            />
                        </GoogleMap>
                    </Box> : <></>
                }
                <Box 
                   position='absolute' 
                   top={0}
                   left={350}
                   zIndex={1}

                >
                    <Autocomplete
                        freeSolo
                        options={locationList.map((location) => location.location)}
                        renderInput={(params) => (
                            <TextField 
                                {...params}
                                label="Origin"
                                margin="normal"
                                variant="outlined"
                                InputProps={{...params.InputProps, type: 'search'}}
                                onChange={(e) => {setOriginLocate(e.target.value)}}
                            />
                        )}
                    />
                    <Autocomplete
                        freeSolo
                        options={locationList.map((location) => location.location)}
                        renderInput={(params) => (
                            <TextField 
                                {...params}
                                label="Destination"
                                margin="normal"
                                variant="outlined"
                                InputProps={{...params.InputProps, type: 'search'}}
                                onChange={(e) => {setDestinationLocate(e.target.value)}}
                            />
                        )}
                    />   
                    <ButtonGroup>
                        {/* <Button onclick={calculateRoute}>Calculate</Button> */}
                        <Button onClick={() => setOpenInfo(true)}>Calculate</Button>
                        <Button onClick={() => setOpenInfo(false)}>Clear</Button>
                    </ButtonGroup>
                </Box>
                {
                    openInfo ? 
                    <Box color="blue"
                        display="flex"
                        flexDirection="column"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Typography style={{fontSize: 18, fontWeight: 500}}>Distance: 5km</Typography>
                        <Typography style={{fontSize: 18, fontWeight: 500}}>Duration: 10min</Typography>
                        <Typography style={{fontSize: 18, fontWeight: 500}}>Price: 25000đ</Typography>
                    </Box> : <></>
                }
        </Box>
        {
            openInfo ? 
            <Grid container justifyContent="center" style={{marginTop: 10}}>
                <Button style={{border: "solid 2px", borderRadius: 30, backgroundColor: "green", width: 400}} 
                onClick={() => {setStatusShip(true); setLoaded(!loaded)}}
                ><span style={{}}>Book a drone for this order</span>
                </Button>
            </Grid> : <></>
        }
        
            
        <Grid container justifyContent="center" style={{padding: 15}}>
            { !statusShip || !loaded ? <Typography style={{color:"red", fontSize: 22, fontWeight: 800}}>Đơn hàng này chưa được vận chuyển </Typography> : <Typography><span style={{color:"green", fontSize: 22, fontWeight: 800}}>Đơn hàng đang được vận chuyển</span> <Link href="/order/track-drone" color="red">Xem hành trình<LocationOn></LocationOn></Link></Typography> }
        </Grid>
            <DarkBackground disappear={!loaded}>
                <LoadingOverlay 
                    active={true}
                    spinner={true}
                    text="Finding Drone..."
                >
                </LoadingOverlay>
            </DarkBackground>
      </>
	)
}

export default MapBook;
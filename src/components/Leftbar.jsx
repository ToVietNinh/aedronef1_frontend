import { Container, makeStyles, Typography } from "@material-ui/core";
import {
  List,
  ExitToApp,
  Home,
  Settings,
} from "@material-ui/icons";
import { Link, useNavigate } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  container: {
    height: "100vh",
    color: "white",
    paddingTop: theme.spacing(10),
    backgroundColor: theme.palette.primary.main,
    position: "sticky",
    top: 0,
    [theme.breakpoints.up("sm")]: {
      backgroundColor: "white",
      color: "#555",
      border: "1px solid #ece7e7",
    },
  },
  item: {
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing(4),
    [theme.breakpoints.up("sm")]: {
      marginBottom: theme.spacing(3),
      cursor: "pointer",
    },
  },
  icon: {
    marginRight: theme.spacing(1),
    [theme.breakpoints.up("sm")]: {
      fontSize: "18px",
    },
  },
  text: {
    fontWeight: 500,
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
    textDecoration: "none",
    color: "#555",
  },

}));

const Leftbar = () => {
  const classes = useStyles();
  let navigate = useNavigate();
  const redirectToLogin = () => {
    let path = "/login"
    navigate(path)
  }

  return (
    <Container className={classes.container}>
      <div className={classes.item}>
        <Home className={classes.icon} />
        <Typography>
          <Link to="/" className={classes.text}>Dashboard</Link>
        </Typography>
      </div>
      <div className={classes.item}>
        <List className={classes.icon} />
        <Typography>
          <Link to="/order/list" className={classes.text}>List Orders</Link>
        </Typography>
      </div>
      <div className={classes.item}>
        <Settings className={classes.icon} />
        <Typography className={classes.text}>Settings</Typography>
      </div>
      <div className={classes.item} onClick={redirectToLogin}>
        <ExitToApp className={classes.icon} />
        <Typography className={classes.text}>Logout</Typography>
      </div>
    </Container>
  );
};

export default Leftbar;

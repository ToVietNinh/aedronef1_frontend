import { Container, makeStyles } from "@material-ui/core";
import Post from "./Post";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(10),
  },
}));

const Feed = () => {
  const classes = useStyles();
  return (
    <Container className={classes.container}>
      <Post title="Order #9238123" />
      <Post title="Order #9342021" />
      <Post title="Order #9342021" />
      <Post title="Order #9342021" />
      <Post title="Order #9342021" />
      <Post title="Order #9342021" />
    </Container>
  );
};

export default Feed;

import React from "react";
import { Routes, Route } from "react-router-dom";

import Login from "../pages/Login";
import Home from "../pages/Home"
import ListOrders from "../pages/ListOrders"
import ViewOrder from "../pages/ViewOrder";
import TrackDroneOrder from "../pages/TrackDroneOrder"

  
const Router = () => {
  return (
    <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/order/list" element={<ListOrders />} />
        <Route path="/order/view" element={<ViewOrder />} />
        <Route path="/order/track-drone" element={<TrackDroneOrder />} />
    </Routes>
  );
};

export default Router;
